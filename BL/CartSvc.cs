﻿using CartService.DataAccess;
using CartService.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CartService.BL
{
    public class CartSvc
    {
        private readonly CartRepo _cartRepo;
        private readonly RabbitMQPublisher _publisher;

        public CartSvc(CartRepo cartRepo, RabbitMQPublisher publisher)
        {
            _cartRepo = cartRepo;
            _publisher = publisher;
        }

       
        public Cart GetCart(int Id)
        {
            Cart cart = _cartRepo.GetCart(Id);
            return cart;
        }

        public List<Cart> GetCarts()
        {
            List<Cart> carts = _cartRepo.GetCarts();
            return carts;
        }


        public Cart PostCart(Cart cart)
        {
            cart.OrderStatus = "INITIATED";
            Cart cartAdded = _cartRepo.PostCart(cart);

            _publisher.Publish(cart, "cart", "INITIATED", "orders");

            return cartAdded;
        }


        public void UpdateOrderStatus(Cart order)
        {
            _cartRepo.UpdateOrderStatus(order);
        }
    }
}
