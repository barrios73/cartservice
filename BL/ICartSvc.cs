﻿using CartService.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CartService.BL
{
    public interface ICartSvc
    {

        public Cart GetCart(int Id);

        public List<Cart> GetCarts();

        public Cart PostCart(Cart cart);

        public void UpdateOrderStatus(Cart order);

    }
}
