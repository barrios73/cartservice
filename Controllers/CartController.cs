﻿using CartService.BL;
using CartService.EntityModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CartService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly CartSvc _cartSvc;
      
        public CartController(CartSvc cartSvc)
        {
            _cartSvc = cartSvc;
        }


        [HttpGet("GetService")]
        public string GetCartService()
        {
            return "Cart service running";
        }


        [HttpGet]
        public ActionResult<List<Cart>> GetCarts()
        {
            try
            {
                List<Cart> carts = _cartSvc.GetCarts();
                return Ok(carts);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error getting all cart information: " + ex.Message);
            }
        }


        [HttpGet("{Id}")]
        public ActionResult<Cart> GetCart(int Id)
        {
            try
            {
                Cart cart = _cartSvc.GetCart(Id);
                return Ok(cart);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error getting all cart information: " + ex.Message);
            }
        }

        //add order
        [HttpPost]
        public ActionResult<Cart> Post(Cart cart)
        {
            try
            {
                Cart cartAdded = _cartSvc.PostCart(cart);
                return Ok(cartAdded);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error getting all cart information: " + ex.Message);
            }

        }



    }
}
