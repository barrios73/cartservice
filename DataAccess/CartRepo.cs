﻿using CartService.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CartService.DataAccess
{
    
    public class CartRepo
    {
        private readonly CartDbContext _ctx;

        public CartRepo(CartDbContext ctx)
        {
            _ctx = ctx;
        }


        public Cart GetCart(int Id)
        {
            Cart cart = _ctx.Carts.Where(x => x.OrderId == Id).FirstOrDefault();
            return cart;
        }


        public List<Cart> GetCarts()
        {
            List<Cart> carts = _ctx.Carts.ToList<Cart>();
            return carts;
        }

        public Cart PostCart(Cart cart)
        {
            _ctx.Carts.Add(cart);
            _ctx.SaveChanges();
            return cart;
        }


        public void UpdateOrderStatus(Cart order)
        {
            Cart orderUpdate = _ctx.Carts.Where(x => x.CartId == order.CartId && x.OrderId == order.OrderId).FirstOrDefault();
            if (order.OrderStatus == "FAILED")
            {
                orderUpdate.OrderStatus = "FAILED";
            }
            else
            {
                orderUpdate.OrderStatus = "PASSED";
            }

            _ctx.Carts.Update(orderUpdate);
            _ctx.SaveChanges();

        }   

    }
}
