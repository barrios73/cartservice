﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CartService.EntityModel
{
    public class Cart
    {
        [Key]
        public int Id { get; set; }
        public int CartId { get; set; }
        public int OrderId { get; set; }
        public string OrderStatus { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public int Total { get; set; }

    }
}
