﻿using CartService.EntityModel;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace CartService
{
    public class RabbitMQPublisher
    {
        public void Publish(Cart order, string exchange, string routingKey, string queue)
        {
            var factory = new ConnectionFactory()
            {                
                HostName = Environment.GetEnvironmentVariable("RABBITMQ_HOST"),
                Port = Convert.ToInt32(Environment.GetEnvironmentVariable("RABBITMQ_PORT"))
            };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: exchange,
                            type: ExchangeType.Direct,
                            durable: true,
                            autoDelete: false);

                
                    channel.QueueDeclare(queue: queue,
                                         durable: true,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    channel.QueueBind(queue: queue,
                                   exchange: exchange,
                                   routingKey: routingKey);
                

                var body = JsonSerializer.SerializeToUtf8Bytes(order);

                channel.BasicPublish(exchange: exchange,
                                     routingKey: routingKey,
                                     basicProperties: null,
                                     body: body);
            }

        }


    }
}
